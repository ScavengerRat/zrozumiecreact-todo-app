import { useState } from "react";
import styles from "./App.module.css";
import { Form } from "./components/form/Form";
import { ToDoItem } from "./components/todoitem/ToDoItem";
import { getSubheading } from "./components/utils/getSubheading";
import { ReactSortable } from "react-sortablejs";

const initialTodos = [
  { name: "Wyrzucić śmiaci", done: true, id: 1 },
  { name: "Zapłacić rachunki", done: false, id: 2 },
];

function App() {
  const [isFormShown, setIsFormShown] = useState(false);
  const [todos, setTodos] = useState(initialTodos);

  const onFormSubmit = (newTodoName) => {
    setTodos((prevState) => [
      ...prevState,
      {
        name: newTodoName,
        done: false,
        // id: prevState.length === 0 ? 0 : prevState.at(-1).id + 1,
        id: Math.random(),
      },
    ]);
    setIsFormShown(false);
  };

  const onDeleteButtonClick = (id) => {
    setTodos((prevState) => prevState.filter((todo) => todo.id !== id));
  };

  const onDoneButtonClick = (id) => {
    setTodos((prevState) =>
      prevState.map((todo) => (todo.id !== id ? todo : { ...todo, done: true }))
    );
  };

  return (
    <div className={styles.container}>
      <header className={styles.header}>
        <div>
          <h1>Todo App</h1>
          <h2>
            {todos.length} {getSubheading(todos.length)}
          </h2>
        </div>
        <button
          onClick={() => setIsFormShown(!isFormShown)}
          className={styles.button}
        >
          +
        </button>
      </header>
      {isFormShown && <Form onFormSubmit={onFormSubmit} />}
      <ul>
        <ReactSortable list={todos} setList={setTodos}>
          {todos.map((item) => (
            <ToDoItem
              key={item.id}
              name={item.name}
              done={item.done}
              id={item.id}
              onDeleteButtonClick={onDeleteButtonClick}
              onDoneButtonClick={onDoneButtonClick}
            />
          ))}
        </ReactSortable>
      </ul>
    </div>
  );
}

export default App;
