import { Button } from "../button/Button";
import styles from "./ToDoItem.module.css";

export const ToDoItem = ({
  name,
  done,
  id,
  onDeleteButtonClick,
  onDoneButtonClick,
}) => {
  return (
    <li className={styles.item}>
      <span className={`${styles.name} ${done && styles.done}`}>{name}</span>
      {!done && <Button onClick={() => onDoneButtonClick(id)}>Zrobione</Button>}
      <Button onClick={() => onDeleteButtonClick(id)}>Usuń</Button>
    </li>
  );
};
