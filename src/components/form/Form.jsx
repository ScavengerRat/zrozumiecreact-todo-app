import { useState } from "react";
import { Button } from "../button/Button";
import styles from "./Form.module.css";

export const Form = ({ onFormSubmit }) => {
  const [inputValue, setInputValue] = useState("");

  const addTodo = (e) => {
    e.preventDefault();

    onFormSubmit(inputValue);
    setInputValue("");
  };

  return (
    <form onSubmit={addTodo} className={styles.form}>
      <input
        className={styles.input}
        type="text"
        value={inputValue}
        onChange={(e) => setInputValue(e.target.value)}
      />
      <Button type="submit">dodaj</Button>
    </form>
  );
};
