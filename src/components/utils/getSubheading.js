export const getSubheading = (numOfTasks) => {
  switch (true) {
    case numOfTasks > 4:
      return "zadań";
    case numOfTasks > 1:
      return "zadania";
    case numOfTasks === 1:
      return "zadanie";
    default:
      return "Brak zadań";
  }
};
